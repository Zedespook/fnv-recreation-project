# Welcome

Some of you can agree with me, that Fallout New Vegas is the text-book of roleplaying games.
After playing it through with all of it’s DLC’s, I got inspired to make this series showing
you how games like these are made through a recreated source code of the game.

This is the first episode of Recreation, and I'll show you how an RPG like Fallout New Vegas
has been made. I only use free and open-source software, so you can download it to your
Windows, Linux, and Mac systems. All the source and assets are available in this repo.

For the game, I'll try not to spoil the game, but I will have to reference some parts. Just
to be warned. It's all for the sake of teaching.

# Movement

The movement in Fallout New Vegas is so basic that it actually comes off lazy. It's been made
with the same engine, that powered Fallout 3, and with it came all the movement bugs too.

This is how a really basic movement system work:
1. Get where the player is pushing a controller stick, or which direction buttons are pushed.
2. Use the information to determine the direction the player wants to go.
3. Move the player in that determined direction.
*For many games, just like with New Vegas, devs add speed gradually until the player reaches
the top speed value. This to make the movement smoother.*

And this is how a very basic jump looks like:
1. Add a "jump force" value to the player until it reaches the peak of the jump.
2. When the peak of the jump is reached, apply force to get them down.

These created a lot of bugs. Maybe the devs didn't realize the GRAVITY of the situation.
The reason I know this is the case, is because while I was coding my prototype movement
script, the same movement bugs happened to me. What bugs am I talking about?
- When jumping, bonking your head doesn't make you fall. 
*Because, the character still tries to reach the peak of the jump.*
- When running against the wall, the player still gains acceleration. 
*Because this simple implementation doesn't have a clue about collisions.*

The reason I don't understand why they didn't fix this is, that mods can fix it, and also
even I could code the solution for this. The simple and lazy solution to head bonking is that
you put a logical collider to the head hitbox. When something hits that collider, the player
falls. For movement, same, logical collider around the body, if there is wall, the player's
velocity should be suppressed.

# World building

Obsidian knew that for a good RPG you need two things:
1. A world.
2. Ways to interact with the world.

For the world, there are tools to make landscapes like we see in the Fallout New Vegas. One
that was used were height maps.

*This is for my YouTube script only.* 
What you see right here is the official, or a near identical to the one used in the
game. Upon importing it looks even close enough.

Height maps work like this. For every light pixel on the image, the terrain goes higher, for
every dark one, the terrain goes lower. This way you can use different tools to make landscapes
like TerreSculptor, Blender and more.

I have very little experience with mapping, so let's move on to how to interact with this world.

# Event system

Every RPG needs an event system. An event system, if you didn't know, is a way to program 
dialogues, quests, skill checks, reputation with factions, doors, and so much more.

Imagine an event system as a programming language for writers. It provides them with the ease
to make a character say kind things to you, if you acted towards them in a positive, and act
hostile, if you were mean to them.

This is a simple way to describe it, but as I introduced it, it can be used for many things.
Like to check if you have a key for a door. Have enough skill points to complete a speech check
and so on. It is to simplify and boost productivity, since there is no other clear cut system
to handle world interaction than an event system.

For example, quests have four event triggers:
1. Not started - The player has yet to trigger the quest.
2. Ongoing - The player started the quest, but yet triggered the end.
3. Completed - The player triggered the end of the quest.
4. Failed - The player cannot reach the end trigger for the quest.

Each providing you with different dialogue options at different NPCs.

# TODO
- Companions, allies, enemies
- Daily routines
- Leveling
